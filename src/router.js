import Vue from 'vue'
import Router from 'vue-router'
import firebase from 'firebase'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Dashboard from './views/Dashboard.vue'
import Blog from './views/blog.vue'
import Entrada from './components/entrada.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {path: '/', name: 'home', component: Home },
    {path: '/about', name: 'about', component: () => import('./views/About.vue')},
    {path: '/Login', name: 'Login', component: Login},
    {path: '/blog', name: 'blog', component: Blog},
    {path: '/blog/:id',name: "entrada", component: Entrada},
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      beforeEnter:(to,from,next)=>{
        const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
        const currentUser = firebase.auth().currentUser

        if (requiresAuth && !currentUser) {
          next('/login')
        } else if (requiresAuth && currentUser) {
          next()
        } else {
          next()
        }
      },
      meta: {
        requiresAuth: true
      }
    }
  ]
})

