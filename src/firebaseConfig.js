import firebase from 'firebase'
import 'firebase/firestore'

const config = {
    apiKey: "AIzaSyAZUXER8ynLqn9aYXdwE905-Z93zZ6dIA4",
    authDomain: "website-c8292.firebaseapp.com",
    databaseURL: "https://website-c8292.firebaseio.com",
    projectId: "website-c8292",
    storageBucket: "website-c8292.appspot.com",
    messagingSenderId: "146200352318"
  };
firebase.initializeApp(config);

const db = firebase.firestore()
const auth = firebase.auth()
const currentUser = auth.currentUser

// firebase collections
const usersCollection = db.collection('users')
const postsCollection = db.collection('entradas')
const commentsCollection = db.collection('cards')
const likesCollection = db.collection('likes')

export {
    db,
    auth,
    currentUser,
    usersCollection,
    postsCollection,
    commentsCollection,
    likesCollection
}